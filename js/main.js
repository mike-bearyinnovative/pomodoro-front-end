function getMeta(){
  var message  = {
    "event": "getMeta",
    "data": {}
  };

  chrome.runtime.sendMessage(message, function(response) {
    console.log(response);
  });
};

$(document).ready(function(){
  // TODO: find better way to wait document ready
  setTimeout(function(){
    $('div[role="textbox"').addClass("pomodoro-textbox");

    getMeta();

  }, 2000);
});
