var bearyChatBaseUrl = "https://api.bearychat.com/v1";

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
  console.log("Received message in background:", message);
  var event = message.event;

  // NOTICE: "getMeta" is just a demo for sending message from content script to background,
  // Should use BearyChat API in content script as the context(such as cookie) is shared in the content script.
  // Should call robot's API in background or it will cause CORS problem
  if (event == "getMeta") {
    var url = bearyChatBaseUrl + "/meta";

    $.get(url, function(data){
      sendResponse(data);
    });

    // return true for sending request asynchronously,
    // more info: https://developer.chrome.com/extensions/messaging#simple
    return true;
  }
});
